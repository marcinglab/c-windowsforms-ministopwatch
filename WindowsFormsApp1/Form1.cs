﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public short h, m, s;
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            increaseSec();
            showTime();
        }

        private void increaseSec()
        {
            if (s == 59)
            {
                s = 0;
                increaseMin();
            }
            else
            {
                s++;
                //Thread.Sleep(1000);
            }
        }

        private void increaseMin()
        {
            if (m == 59)
            {
                m = 0;
                increaseHours();
            }
            else
            {
                m++;
            }
        }

        private void increaseHours()
        {
            if (h == 59)
            {
                h = 0;
                increaseSec();
            }
            else
            {
                h++;
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Start.Enabled = false;
            timer1.Enabled = true;
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Start.Enabled = true;
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            h = 0;
            m = 0;
            s = 0;

            showTime();
            Start.Enabled = true;
        }

        private void showTime()
        {
            hoursText.Text = h.ToString("00");
            minutesText.Text = m.ToString("00");
            secText.Text = s.ToString("00");
        }
    }
}
